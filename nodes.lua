--
-- LEAVES
--

-- DEFAULT LEAVES
-- minetest.override_item("default:leaves", {
-- 	tiles = {"xmas_default_leaves.png"},
-- })

-- ASPEN LEAVES
-- minetest.override_item("default:aspen_leaves", {
-- 	tiles = {"xmas_aspen_leaves.png"},
-- })

-- DEFAULT SHRUB
minetest.override_item("default:dry_shrub", {
	tiles = {"xmas_default_dry_shrub.png"},
	inventory_image = "xmas_default_dry_shrub.png",
	wield_image = "xmas_default_dry_shrub.png",
	place_param2 = 0
})

-- GLASS
minetest.override_item("default:glass", {
	tiles = {"xmas_glass.png", "xmas_glass_detail.png"},
})

-- OBSIDIAN GLASS
minetest.override_item("default:obsidian_glass", {
	tiles = {"xmas_obsidian_glass.png", "xmas_glass_detail.png"},
})

-- XPANE GLASS FLAT
minetest.override_item("xpanes:pane_flat", {
	tiles = {"xpanes_white.png", "xmas_glass.png", "xmas_glass.png"},
	inventory_image = "xmas_glass.png",
	wield_image = "xmas_glass.png",
})

--
-- DOORS
--

-- DOOR WOOD
minetest.override_item("doors:door_wood", {
	inventory_image = "xmas_doors_item_wood.png",
})

minetest.override_item("doors:door_wood_a", {
	tiles = {{name = "xmas_door_wood.png", backface_culling = true}},
})

minetest.override_item("doors:door_wood_b", {
	tiles = {{name = "xmas_door_wood.png", backface_culling = true}},
})

-- DOOR STEEL
minetest.override_item("doors:door_steel", {
	inventory_image = "xmas_doors_item_steel.png",
})

minetest.override_item("doors:door_steel_a", {
	tiles = {{name = "xmas_door_steel.png", backface_culling = true}},
})

minetest.override_item("doors:door_steel_b", {
	tiles = {{name = "xmas_door_steel.png", backface_culling = true}},
})

--
-- FARMING
--

-- BREAD
minetest.override_item("farming:bread", {
	description = "Ginger Bread Man",
	inventory_image = "xmas_ginger_bread_man.png",
})

-- APPLE
minetest.override_item("default:apple", {
	description = "Candy Cain",
	tiles = {"xmas_candy_cain.png"},
	inventory_image = "xmas_candy_cain.png",
	wield_image = "xmas_candy_cain.png",
})

--
-- TREES
--

-- DEFAULT TREE
-- minetest.override_item("default:tree", {
-- 	tiles = {"default_tree_top.png", "default_tree_top.png", "xmas_tree.png"},
-- })

--
-- FENCES / GATES
--

-- FENCE WOOD
-- minetest.override_item("default:fence_wood", {
-- 	tiles = {"xmas_fence_wood.png"},
-- 	inventory_image = "default_fence_overlay.png^xmas_fence_wood.png^default_fence_overlay.png^[makealpha:255,126,126",
-- 	wield_image = "default_fence_overlay.png^xmas_fence_wood.png^default_fence_overlay.png^[makealpha:255,126,126",
-- })

-- GATE WOOD
-- minetest.override_item("doors:gate_wood_open", {
-- 	tiles = {"xmas_wood.png"},
-- })
-- minetest.override_item("doors:gate_wood_closed", {
-- 	tiles = {"xmas_wood.png"},
-- })

-- FENCE PINE WOOD
-- minetest.override_item("default:fence_pine_wood", {
-- 	tiles = {"xmas_fence_pine_wood.png"},
-- 	inventory_image = "default_fence_overlay.png^xmas_fence_pine_wood.png^default_fence_overlay.png^[makealpha:255,126,126",
-- 	wield_image = "default_fence_overlay.png^xmas_fence_pine_wood.png^default_fence_overlay.png^[makealpha:255,126,126",
-- })

-- GATE PINE WOOD
-- minetest.override_item("doors:gate_pine_wood_open", {
-- 	tiles = {"xmas_pine_wood.png"},
-- })
-- minetest.override_item("doors:gate_pine_wood_closed", {
-- 	tiles = {"xmas_pine_wood.png"},
-- })

-- FENCE ACACIA WOOD
-- minetest.override_item("default:fence_acacia_wood", {
-- 	tiles = {"xmas_fence_acacia_wood.png"},
-- 	inventory_image = "default_fence_overlay.png^xmas_fence_acacia_wood.png^default_fence_overlay.png^[makealpha:255,126,126",
-- 	wield_image = "default_fence_overlay.png^xmas_fence_acacia_wood.png^default_fence_overlay.png^[makealpha:255,126,126",
-- })

-- GATE ACACIA WOOD
-- minetest.override_item("doors:gate_acacia_wood_open", {
-- 	tiles = {"xmas_acacia_wood.png"},
-- })
-- minetest.override_item("doors:gate_acacia_wood_closed", {
-- 	tiles = {"xmas_acacia_wood.png"},
-- })

-- FENCE JUNGLEWOOD
-- minetest.override_item("default:fence_junglewood", {
-- 	tiles = {"xmas_fence_junglewood.png"},
-- 	inventory_image = "default_fence_overlay.png^xmas_fence_junglewood.png^default_fence_overlay.png^[makealpha:255,126,126",
-- 	wield_image = "default_fence_overlay.png^xmas_fence_junglewood.png^default_fence_overlay.png^[makealpha:255,126,126",
-- })

-- GATE JUNGLEWOOD
-- minetest.override_item("doors:gate_junglewood_open", {
-- 	tiles = {"xmas_junglewood.png"},
-- })
-- minetest.override_item("doors:gate_junglewood_closed", {
-- 	tiles = {"xmas_junglewood.png"},
-- })

-- FENCE ASPEN WOOD
-- minetest.override_item("default:fence_aspen_wood", {
-- 	tiles = {"xmas_fence_aspen_wood.png"},
-- 	inventory_image = "default_fence_overlay.png^xmas_fence_aspen_wood.png^default_fence_overlay.png^[makealpha:255,126,126",
-- 	wield_image = "default_fence_overlay.png^xmas_fence_aspen_wood.png^default_fence_overlay.png^[makealpha:255,126,126",
-- })

-- GATE ASPEN WOOD
-- minetest.override_item("doors:gate_aspen_wood_open", {
-- 	tiles = {"xmas_aspen_wood.png"},
-- })
-- minetest.override_item("doors:gate_aspen_wood_closed", {
-- 	tiles = {"xmas_aspen_wood.png"},
-- })

--
-- LADDERS
--

-- LADDER WOOD
-- minetest.override_item("default:ladder_wood", {
-- 	tiles = {"xmas_ladder_wood.png"},
-- 	inventory_image = "xmas_ladder_wood.png",
-- 	wield_image = "xmas_ladder_wood.png",
-- })

-- LADDER STEEL
-- minetest.override_item("default:ladder_steel", {
-- 	tiles = {"xmas_ladder_steel.png"},
-- 	inventory_image = "xmas_ladder_steel.png",
-- 	wield_image = "xmas_ladder_steel.png",
-- })

--
-- WOOD (PLANKS)
--

-- DEFAULT WOOD
-- minetest.override_item("default:wood", {
-- 	tiles = {"xmas_wood.png"},
-- })

-- STAIRS WOOD
-- minetest.override_item("stairs:stair_wood", {
-- 	tiles = {"xmas_wood.png"},
-- })

-- STAIRS WOOD - INNER
-- minetest.override_item("stairs:stair_inner_wood", {
-- 	tiles = {"xmas_wood.png"},
-- })

-- STAIRS WOOD - OUTER
-- minetest.override_item("stairs:stair_outer_wood", {
-- 	tiles = {"xmas_wood.png"},
-- })

-- STAIRS WOOD - SLAB
-- minetest.override_item("stairs:slab_wood", {
-- 	tiles = {"xmas_wood.png"},
-- })

-- DEFAULT PINE WOOD
-- minetest.override_item("default:pine_wood", {
-- 	tiles = {"xmas_pine_wood.png"},
-- })

-- STAIRS PINE WOOD
-- minetest.override_item("stairs:stair_pine_wood", {
-- 	tiles = {"xmas_pine_wood.png"},
-- })

-- STAIRS PINE WOOD - INNER
-- minetest.override_item("stairs:stair_inner_pine_wood", {
-- 	tiles = {"xmas_pine_wood.png"},
-- })

-- STAIRS PINE WOOD - OUTER
-- minetest.override_item("stairs:stair_outer_pine_wood", {
-- 	tiles = {"xmas_pine_wood.png"},
-- })

-- STAIRS PINE WOOD - SLAB
-- minetest.override_item("stairs:slab_pine_wood", {
-- 	tiles = {"xmas_pine_wood.png"},
-- })

-- DEFAULT ACACIA WOOD
-- minetest.override_item("default:acacia_wood", {
-- 	tiles = {"xmas_acacia_wood.png"},
-- })

-- STAIRS ACACIA WOOD
-- minetest.override_item("stairs:stair_acacia_wood", {
-- 	tiles = {"xmas_acacia_wood.png"},
-- })

-- STAIRS ACACIA WOOD - INNER
-- minetest.override_item("stairs:stair_inner_acacia_wood", {
-- 	tiles = {"xmas_acacia_wood.png"},
-- })

-- STAIRS ACACIA WOOD - OUTER
-- minetest.override_item("stairs:stair_outer_acacia_wood", {
-- 	tiles = {"xmas_acacia_wood.png"},
-- })

-- STAIRS ACACIA WOOD - SLAB
-- minetest.override_item("stairs:slab_acacia_wood", {
-- 	tiles = {"xmas_acacia_wood.png"},
-- })

-- DEFAULT JUNGLEWOOD
-- minetest.override_item("default:junglewood", {
-- 	tiles = {"xmas_junglewood.png"},
-- })

-- STAIRS JUNGLEWOOD
-- minetest.override_item("stairs:stair_junglewood", {
-- 	tiles = {"xmas_junglewood.png"},
-- })

-- STAIRS JUNGLEWOOD - INNER
-- minetest.override_item("stairs:stair_inner_junglewood", {
-- 	tiles = {"xmas_junglewood.png"},
-- })

-- STAIRS JUNGLEWOOD - OUTER
-- minetest.override_item("stairs:stair_outer_junglewood", {
-- 	tiles = {"xmas_junglewood.png"},
-- })

-- STAIRS JUNGLEWOOD - SLAB
-- minetest.override_item("stairs:slab_junglewood", {
-- 	tiles = {"xmas_junglewood.png"},
-- })

-- DEFAULT ASPEN WOOD
-- minetest.override_item("default:aspen_wood", {
-- 	tiles = {"xmas_aspen_wood.png"},
-- })

-- STAIRS ASPEN WOOD
-- minetest.override_item("stairs:stair_aspen_wood", {
-- 	tiles = {"xmas_aspen_wood.png"},
-- })

-- STAIRS ASPEN WOOD - INNER
-- minetest.override_item("stairs:stair_inner_aspen_wood", {
-- 	tiles = {"xmas_aspen_wood.png"},
-- })

-- STAIRS ASPEN WOOD - OUTER
-- minetest.override_item("stairs:stair_outer_aspen_wood", {
-- 	tiles = {"xmas_aspen_wood.png"},
-- })

-- STAIRS ASPEN WOOD - SLAB
-- minetest.override_item("stairs:slab_aspen_wood", {
-- 	tiles = {"xmas_aspen_wood.png"},
-- })

--
-- STONE
--

-- DEFAULT STONE BRICK
-- minetest.override_item("default:stonebrick", {
-- 	tiles = {"xmas_stone_brick.png"},
-- })

-- STAIRS STAIR STONE BRICK
-- minetest.override_item("stairs:stair_stonebrick", {
-- 	tiles = {"xmas_stone_brick.png"},
-- })

-- STAIRS STAIR STONE BRICK - INNER
-- minetest.override_item("stairs:stair_inner_stonebrick", {
-- 	tiles = {"xmas_stone_brick.png"},
-- })

-- STAIRS STAIR STONE BRICK - OUTER
-- minetest.override_item("stairs:stair_outer_stonebrick", {
-- 	tiles = {"xmas_stone_brick.png"},
-- })

-- STAIRS SLAB STONE BRICK - SLAB
-- minetest.override_item("stairs:slab_stonebrick", {
-- 	tiles = {"xmas_stone_brick.png"},
-- })

-- DEFAULT COBBBLE
-- minetest.override_item("default:cobble", {
-- 	tiles = {"xmas_cobble.png"},
-- })

-- STAIRS STAIR COBBBLE
-- minetest.override_item("stairs:stair_cobble", {
-- 	tiles = {"xmas_cobble.png"},
-- })

-- STAIRS STAIR COBBBLE - INNER
-- minetest.override_item("stairs:stair_inner_cobble", {
-- 	tiles = {"xmas_cobble.png"},
-- })

-- STAIRS STAIR COBBBLE - OUTER
-- minetest.override_item("stairs:stair_outer_cobble", {
-- 	tiles = {"xmas_cobble.png"},
-- })

-- STAIRS SLAB COBBBLE - SLAB
-- minetest.override_item("stairs:slab_cobble", {
-- 	tiles = {"xmas_cobble.png"},
-- })

-- STAIRS SLAB COBBBLE
-- minetest.override_item("walls:cobble", {
-- 	tiles = {"xmas_cobble.png"},
-- })

--
-- GRASS / DIRT
--

-- DIRT WITH GRASS
-- minetest.override_item("default:dirt_with_grass", {
-- 	tiles = {"xmas_grass.png", "default_dirt.png",
-- 		{name = "default_dirt.png^xmas_grass_side.png",
-- 			tileable_vertical = false}},
-- })

--
-- CHESTS
--

-- DEFAULT CHEST
minetest.override_item("default:chest", {
	tiles = {
		"xmas_chest_top_2.png",
		"xmas_chest_top_2.png",
		"xmas_chest_side_2.png",
		"xmas_chest_side_2.png",
		"xmas_chest_back_2.png",
		"xmas_chest_front_2.png"
	},
})

minetest.override_item("default:chest_open", {
	tiles = {
		"xmas_chest_top_2.png",
		"xmas_chest_top_2.png",
		"xmas_chest_side_2.png",
		"xmas_chest_side_2.png",
		"xmas_chest_front_2.png",
		"xmas_chest_inside_2.png"
	},
})

-- LOCKED CHEST
minetest.override_item("default:chest_locked", {
	tiles = {
		"xmas_chest_top.png",
		"xmas_chest_top.png",
		"xmas_chest_side.png",
		"xmas_chest_side.png",
		"xmas_chest_side.png",
		"xmas_chest_lock.png"
	},
})

minetest.override_item("default:chest_locked_open", {
	tiles = {
		"xmas_chest_top.png",
		"xmas_chest_top.png",
		"xmas_chest_side.png",
		"xmas_chest_side.png",
		"xmas_chest_lock.png",
		"xmas_chest_inside.png"
	},
})

-- PROTECTED CHEST
if (minetest.get_modpath("protector")) then
	-- CHEST
	minetest.override_item("protector:chest", {
		tiles = {
			"xmas_chest_protected_top.png",
			"xmas_chest_protected_top.png",
			"xmas_chest_protected_side.png",
			"xmas_chest_protected_side.png",
			"xmas_chest_protected_side.png",
			"xmas_chest_protected_front.png"
		},
	})

	-- PROTECT BLOCK
	minetest.override_item("protector:protect", {
		tiles = {
			"xmas_protector_protect_top.png",
			"xmas_protector_protect_top.png",
			"xmas_protector_protect_side.png",
			"xmas_protector_protect_side.png",
			"xmas_protector_protect_side.png",
			"xmas_protector_protect_side.png"
		},
	})

	-- PROTECT BLOCK PVP
	minetest.override_item("protector:protect_pvp", {
		tiles = {
			"xmas_protector_protect_pvp_top.png",
			"xmas_protector_protect_pvp_top.png",
			"xmas_protector_protect_pvp_side.png",
			"xmas_protector_protect_pvp_side.png",
			"xmas_protector_protect_pvp_side.png",
			"xmas_protector_protect_pvp_side.png"
		},
	})
end

-- OBSIDIAN MESE CHEST
-- @TODO - right side being flipped up side down when chest open
-- if (minetest.get_modpath("obsidianmese")) then
-- 	minetest.override_item("obsidianmese:chest", {
-- 		tiles = {
-- 			"xmas_obsidianmese_chest_top.png",
-- 			"xmas_obsidianmese_chest_top.png",
-- 			"xmas_obsidianmese_chest_side.png",
-- 			"xmas_obsidianmese_chest_side.png",
-- 			"xmas_obsidianmese_chest_side.png",
-- 			"xmas_obsidianmese_chest_front.png"
-- 		},
-- 	})

-- 	minetest.override_item("obsidianmese:chest_open", {
-- 		tiles = {
-- 			"xmas_obsidianmese_chest_top.png",
-- 			"xmas_obsidianmese_chest_top.png",
-- 			"xmas_obsidianmese_chest_side.png",
-- 			"xmas_obsidianmese_chest_side.png",
-- 			"xmas_obsidianmese_chest_front.png",
-- 			"xmas_obsidianmese_chest_inside.png"
-- 		},
-- 	})
-- end

-- BASIC MACHINES - LIGHT ON
-- if (minetest.get_modpath("basic_machines")) then
-- 	minetest.override_item("basic_machines:light_on", {
-- 		tiles = {"xmas_light.png"}
-- 	})
-- end

-- BASIC MACHINES - LIGHT OFF
-- if (minetest.get_modpath("basic_machines")) then
-- 	minetest.override_item("basic_machines:light_off", {
-- 		tiles = {"xmas_light_off.png"}
-- 	})
-- end

-- MESELAMP
-- minetest.override_item("default:meselamp", {
-- 	tiles = {"xmas_meselamp.png"},
-- })

-- MESELAMP - POST LIGHT
-- minetest.override_item("default:mese_post_light", {
-- 	tiles = {
-- 		"xmas_mese_post_light_top.png",
-- 		"xmas_mese_post_light_top.png",
-- 		"xmas_mese_post_light_side_dark.png",
-- 		"xmas_mese_post_light_side_dark.png",
-- 		"xmas_mese_post_light_side.png",
-- 		"xmas_mese_post_light_side.png"
-- 	},
-- 	wield_image = "xmas_mese_post_light_side.png",
-- })
